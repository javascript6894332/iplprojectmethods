// Import necessary modules and libraries
const matchesPath = "../data/matches.csv"; // Path to the CSV file containing match data
const outputPath = "../public/output/2-matches-won-per-team-per-year.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const matchesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON

function matchesWonPerTeamPerYear() {
  // Read the CSV file and convert it to JSON using csvtojson
  matchesCsv()
    .fromFile(matchesPath)
    .then((matchesJson) => {
      const matchesData = matchesJson; // Store the JSON data representing match information
      function matchesCallBack(accumulator, currentMatch) {
        const season = currentMatch.season;
        const winner = currentMatch.winner;
        if (accumulator[season]) {
          if (!accumulator[season][winner]) {
            accumulator[season][winner] = 1;
          } else {
            accumulator[season][winner] += 1;
          }
        } else {
          accumulator[season] = { [winner]: 1 };
        }
        return accumulator;
      }
      const result = matchesData.reduce(matchesCallBack, {});

      fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(`The file produced an error: ${error}`);
        } else {
          console.log(`Successfully exported output to ${outputPath}`);
        }
      });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
matchesWonPerTeamPerYear();
