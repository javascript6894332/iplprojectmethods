// Import necessary modules and libraries
const deliveriesPath = "../data/deliveries.csv"; // Path to the CSV file containing delivery data

const outputPath =
  "../public/output/8-highest-no-of-times-dismissed-by-player.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const deliveriesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON

function playerDismissedMostbyBowler() {
  deliveriesCsv()
    .fromFile(deliveriesPath)
    .then((deliveriesJson) => {
      const deliveryData = deliveriesJson;
      const batsmanList = {};
      let result = ["x", ["y", 0]];

      result = deliveryData.reduce((updatedResult, delivery) => {
        const batsman = delivery.player_dismissed;
        const bowler = delivery.bowler;
        const isDismissed = delivery.dismissal_kind === "run out" ? 0 : 1;

        if (batsmanList[batsman]) {
          if (batsmanList[batsman][bowler] && isDismissed) {
            batsmanList[batsman][bowler] += 1;
            if (
              updatedResult[1][1] <= batsmanList[batsman][bowler] &&
              batsman
            ) {
              updatedResult = [batsman, [bowler, batsmanList[batsman][bowler]]];
            }
          } else {
            if (isDismissed) {
              batsmanList[batsman][bowler] = 1;
            }
          }
        } else {
          batsmanList[batsman] = {};
          if (isDismissed) {
            batsmanList[batsman][bowler] = 1;
          }
        }

        return updatedResult;
      }, result);

      let name = result[1][0];
      let times = result[1][1];
      result[1] = { name, times };

      fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(`The file produced an error: ${error}`);
        } else {
          console.log(`Successfully exported output to ${outputPath}`);
        }
      });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
playerDismissedMostbyBowler();
