// Import necessary modules and libraries
const deliveriesPath = "../data/deliveries.csv"; // Path to the CSV file containing delivery data
const matchesPath = "../data/matches.csv";
const outputPath = "../public/output/3-extra-runs-per-team.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const deliveriesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON
const matchesCsv = require("csvtojson");

function extraRunsPerTeam2016() {
  deliveriesCsv()
    .fromFile(deliveriesPath)
    .then((deliveriesJson) => {
      const deliveriesData = deliveriesJson;
      matchesCsv()
        .fromFile(matchesPath)
        .then((matchesJson) => {
          const matchData = matchesJson;
          function matchesCallBack(accumulator, currentMatch) {
            if (currentMatch.season == 2016) {
              accumulator[currentMatch.id] = 1;
            }
            return accumulator;
          }
          function deliveriesCallBack(accumulator, currentDelivery) {
            const id = currentDelivery.match_id;
            if (id in matchId) {
              if (accumulator[currentDelivery.bowling_team]) {
                accumulator[currentDelivery.bowling_team] += Number(
                  currentDelivery.extra_runs
                );
              } else {
                accumulator[currentDelivery.bowling_team] = Number(
                  currentDelivery.extra_runs
                );
              }
            }
            return accumulator;
          }
          let matchId = matchData.reduce(matchesCallBack, {});
          const result = deliveriesData.reduce(deliveriesCallBack, {});

          fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
            if (error) {
              console.error(`The file produced an error: ${error}`);
            } else {
              console.log(`Successfully exported output to ${outputPath}`);
            }
          });
        })
        .catch((err) => {
          console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
        });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
extraRunsPerTeam2016();
