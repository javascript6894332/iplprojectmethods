// Import necessary modules and libraries
const deliveriesPath = "../data/deliveries.csv"; // Path to the CSV file containing delivery data
const matchesPath = "../data/matches.csv";
const outputPath = "../public/output/4-top-10-economical-bowlers.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const deliveriesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON
const matchesCsv = require("csvtojson");

function topTenEconomicalBowler15() {
  deliveriesCsv()
    .fromFile(deliveriesPath)
    .then((deliveriesJson) => {
      const deliveriesData = deliveriesJson;
      matchesCsv()
        .fromFile(matchesPath)
        .then((matchesJson) => {
          const matchData = matchesJson;

          function idCallBack(accumulator, currentMatch) {
            if (currentMatch.season == 2015) {
              accumulator[currentMatch.id] = 1;
            }
            return accumulator;
          }
          const id_2015 = matchData.reduce(idCallBack, {});

          function economyCallBack(accumulator, currentDelivery) {
            const bowler = currentDelivery.bowler;
            const runs = currentDelivery.total_runs;
            if (id_2015[currentDelivery.match_id]) {
              if (accumulator[bowler]) {
                accumulator[bowler] += Number(runs);
              } else {
                accumulator[bowler] = Number(runs);
              }
            }
            return accumulator;
          }
          const economy_list = deliveriesData.reduce(economyCallBack, {});

          function oversCallBack(accumulator, currentDelivery) {
            const bowler = currentDelivery.bowler;
            if (id_2015[currentDelivery.match_id]) {
              if (accumulator[bowler]) {
                accumulator[bowler] += 1;
              } else {
                accumulator[bowler] = 1;
              }
            }
            return accumulator;
          }
          const bowlersOvers = deliveriesData.reduce(oversCallBack, {});

          function economyRateCallBack(accumulator, currentBowler) {
            accumulator[currentBowler] =
              economy_list[currentBowler] / (bowlersOvers[currentBowler] / 6);
            return accumulator;
          }

          const bowlersEconomy = Object.keys(bowlersOvers).reduce(
            economyRateCallBack,
            {}
          );

          const economyAscending = Object.entries(bowlersEconomy).sort(
            (a, b) => {
              return a[1] - b[1];
            }
          );

          const result = economyAscending
            .slice(0, 10)
            .map(([name, economy_rate]) => {
              return { name, economy_rate };
            });

          fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
            if (error) {
              console.error(`The file produced an error: ${error}`);
            } else {
              console.log(`Successfully exported output to ${outputPath}`);
            }
          });
        })
        .catch((err) => {
          console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
        });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
topTenEconomicalBowler15();
