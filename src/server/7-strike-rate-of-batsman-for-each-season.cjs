// Import necessary modules and libraries
const deliveriesPath = "../data/deliveries.csv"; // Path to the CSV file containing delivery data
const matchesPath = "../data/matches.csv";
const outputPath = "../public/output/7-strike-rate-of-batsman-for-each-season.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const deliveriesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON
const matchesCsv = require("csvtojson");

function strikeRateOfBatsmanPerYear() {
  deliveriesCsv()
    .fromFile(deliveriesPath)
    .then((deliveriesJson) => {
      const deliveriesData = deliveriesJson;
      matchesCsv()
        .fromFile(matchesPath)
        .then((matchesJson) => {
          const matchData = matchesJson;
          // const result = {};
          function matchCallBack(accumulator, currentMatch) {
            const season = currentMatch.season;
            const matchId = currentMatch.id;
            accumulator[matchId] = season;
            return accumulator;
          }
          const matchYearId = matchData.reduce(matchCallBack, {});

          function deliveryCallBack(accumulator, currentDelivery) {
            const batsman = currentDelivery.batsman;
            const deliveryId = currentDelivery.match_id;
            const year = matchYearId[deliveryId];
            if (batsman in accumulator) {
              const runs =
                Number(currentDelivery.total_runs) -
                Number(currentDelivery.extra_runs);
              if (year in accumulator[batsman]) {
                accumulator[batsman][year][0] += runs;
                accumulator[batsman][year][1] += 1;
              } else {
                accumulator[batsman][year] = [runs, 1];
              }
            } else {
              const runs =
                Number(currentDelivery.total_runs) -
                Number(currentDelivery.extra_runs);
              accumulator[batsman] = {};
              accumulator[batsman][year] = [runs, 1];
            }
            return accumulator;
          }
          const result = deliveriesData.reduce(deliveryCallBack, {});

          Object.keys(result).map((playerName) => {
            result[playerName] = Object.keys(result[playerName]).reduce(
              (acc, iplYear) => {
                acc[iplYear] = Math.round(
                  (result[playerName][iplYear][0] /
                    result[playerName][iplYear][1]) *
                    100
                );
                return acc;
              },
              {}
            );
          });
          fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
            if (error) {
              console.error(`The file produced an error: ${error}`);
            } else {
              console.log(`Successfully exported output to ${outputPath}`);
            }
          });
        })
        .catch((err) => {
          console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
        });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
strikeRateOfBatsmanPerYear();
