// Import necessary modules and libraries
const deliveriesPath = "../data/deliveries.csv"; // Path to the CSV file containing delivery data

const outputPath = "../public/output/9-bowler-with-best-economy.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const deliveriesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON

function bestSuperOverBowler() {
  deliveriesCsv()
    .fromFile(deliveriesPath)
    .then((deliveriesJson) => {
      const deliveriesData = deliveriesJson;

      const superOverBowlers = deliveriesData.reduce((acc, delivery) => {
        const isSuper = Number(delivery.is_super_over);
        const bowler = delivery.bowler;
        const runs = Number(delivery.total_runs);

        if (isSuper) {
          if (bowler in acc) {
            acc[bowler][0] += runs;
            acc[bowler][1] += 1;
          } else {
            acc[bowler] = [runs, 1];
          }
        }

        return acc;
      }, {});
      const result = [
        Object.keys(superOverBowlers).reduce(
          (minBowler, bowlerName) => {
            const average =
              (superOverBowlers[bowlerName][0] * 6) /
              superOverBowlers[bowlerName][1];
            if (average < minBowler.average) {
              minBowler.name = bowlerName;
              minBowler.average = average;
            }
            return minBowler;
          },
          { name: "x", average: 100000 }
        ),
      ];
      fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(`The file produced an error: ${error}`);
        } else {
          console.log(`Successfully exported output to ${outputPath}`);
        }
      });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
bestSuperOverBowler();
