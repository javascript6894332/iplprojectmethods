// Import necessary modules and libraries
const matchesPath = "../data/matches.csv";          // Path to the CSV file containing match data
const outputPath = "../public/output/1-matches-per-year.json";   // Path to the output JSON file
const fs = require("fs");                           // Import the Node.js file system module
const matchesCsv = require("csvtojson");            // Import the csvtojson library for converting CSV to JSON

// Define a function to calculate and export the number of matches per year
function matchesPerYear() {
  // Read the CSV file and convert it to JSON using csvtojson
  matchesCsv()
    .fromFile(matchesPath)
    .then((matchesJson) => {
      const matchesData = matchesJson;  // Store the JSON data representing match information

      // Callback function to calculate the number of matches per year
      function matchesCallBack(accumulator, currentMatch) {
        if (currentMatch.season in accumulator) {
          accumulator[currentMatch.season] += 1;  // Increment the match count for the current year
        } else {
          accumulator[currentMatch.season] = 1;   // Initialize match count for the current year
        }
        return accumulator;
      }

      // Use the reduce function to accumulate the match counts per year
      const result = matchesData.reduce(matchesCallBack, {});

      // Write the results to the specified JSON output file
      fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(`The file produced an error: ${error}`);
        } else {
          console.log(`Successfully exported output to ${outputPath}`);
        }
      });
    })
    .catch((err) => {
      console.error(err);  // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}

// Call the function to calculate and export matches per year
matchesPerYear();
