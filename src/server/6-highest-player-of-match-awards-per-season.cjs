// Import necessary modules and libraries
const matchesPath = "../data/matches.csv"; // Path to the CSV file containing match data
const outputPath = "../public/output/6-highest-player-of-match-awards-per-season.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const matchesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON

function teamWinningTossAndMatch() {
  // Read the CSV file and convert it to JSON using csvtojson
  matchesCsv()
    .fromFile(matchesPath)
    .then((matchesJson) => {
      const matchesData = matchesJson; // Store the JSON data representing match information
      function matchCallBack(accumulator, currentMatch) {
        const mom = currentMatch.player_of_match;
        const year = currentMatch.season;
        if (accumulator[year]) {
          if (accumulator[year][mom]) {
            accumulator[year][mom] += 1;
          } else {
            accumulator[year][mom] = 1;
          }
        } else {
          accumulator[year] = {};
          accumulator[year][mom] = 1;
        }
        return accumulator;
      }
      const allManOfMatch = matchesData.reduce(matchCallBack, {});

      const result = [];
      for (year in allManOfMatch) {
        const descending = Object.entries(allManOfMatch[year]).sort((a, b) => {
          return b[1] - a[1];
        });
        let name = descending[0][0];
        let awards = descending[0][1];
        result.push(year, { name, awards });
      }

      fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(`The file produced an error: ${error}`);
        } else {
          console.log(`Successfully exported output to ${outputPath}`);
        }
      });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
teamWinningTossAndMatch();
