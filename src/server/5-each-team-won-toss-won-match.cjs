// Import necessary modules and libraries
const matchesPath = "../data/matches.csv"; // Path to the CSV file containing match data
const outputPath = "../public/output/5-each-team-won-toss-won-match.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const matchesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON

function teamWinningTossAndMatch() {
  // Read the CSV file and convert it to JSON using csvtojson
  matchesCsv()
    .fromFile(matchesPath)
    .then((matchesJson) => {
      const matchesData = matchesJson; // Store the JSON data representing match information
      function matchCallBack(accumulator, currentMatch) {
        const tossWin = currentMatch.toss_winner;
        const matchWin = currentMatch.winner;
        if (accumulator[tossWin]) {
          if (tossWin === matchWin) {
            accumulator[tossWin] += 1;
          }
        } else {
          if (tossWin === matchWin) {
            accumulator[tossWin] = 1;
          }
        }
        return accumulator;
      }
      const result = matchesData.reduce(matchCallBack, {});

      fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(`The file produced an error: ${error}`);
        } else {
          console.log(`Successfully exported output to ${outputPath}`);
        }
      });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
teamWinningTossAndMatch();
